window._CCSettings = {
    platform: "web-mobile",
    groupList: [
        "default"
    ],
    collisionMatrix: [
        [
            true
        ]
    ],
    hasResourcesBundle: true,
    hasStartSceneBundle: false,
    remoteBundles: [],
    subpackages: [],
    launchScene: "db://assets/scenes/main.fire",
    orientation: "portrait",
    debug: true,
    jsList: [],
    bundleVers: {
        internal: "f1619",
        resources: "6a0ab",
        main: "d2c6d"
    }
};
