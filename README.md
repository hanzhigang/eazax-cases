# Eazax-CCC 示例合集

## 介绍

[Cocos Creator] Eazax-CCC 功能模块示例合集

- 在线预览：[https://ifaswind.gitee.io/eazax-cases](https://ifaswind.gitee.io/eazax-cases)

- Eazax-CCC：[https://gitee.com/ifaswind/eazax-ccc](https://gitee.com/ifaswind/eazax-ccc)

如果此项目对你有帮助，请不要忘记 [![star](https://gitee.com/ifaswind/eazax-cases/badge/star.svg?theme=dark)](https://gitee.com/ifaswind/eazax-cases/stargazers)

> 如有使用上的问题，可以在 gitee 上提 issue 或者添加我的微信 `im_chenpipi` 并留言。



---



## 环境

引擎：Cocos Creator 2.4.3

语言：TypeScript



---



# 菜鸟小栈

我是陈皮皮，这是我的个人公众号，专注但不仅限于游戏开发、前端和后端技术记录与分享。

每一篇原创都非常用心，你的关注就是我原创的动力！

> Input and output.

![](https://gitee.com/ifaswind/image-storage/raw/master/weixin/official-account.png)



---



## 交流群

皮皮创建了一个游戏开发交流群，供大家交流经验、问题求助等。

感兴趣的小伙伴可以添加我微信 `im_chenpipi` 并留言 `加群`。